<img src="https://play-lh.googleusercontent.com/1JBSmrZ2RfPNrqnf9ZlWGgJ8mGEV_-nnvP66ifEbXBqwadbMAqIBrY3vBMJtD0CKll4=s180" alt="logo" width="300px" style="margin-center: 10px;"/>

## Minesweeper


### Description

Minesweeper is a single-player puzzle video game. The objective of the game is to clear a rectangular board containing hidden `mines` or `bombs` without detonating any of them, with help from clues about the number of neighboring mines in each field.
In Minesweeper, mines (that resemble naval mines in the classic theme) are scattered throughout a board, which is divided into cells. Cells have three states: `uncovered, covered and flagged`. A covered cell is blank and clickable, while an uncovered cell is exposed. Flagged cells are those marked by the player to indicate a potential mine location.
A player left-clicks a cell to uncover it. If a player uncovers a mined cell, the game ends, as there is only 1 life per game. Otherwise, the uncovered cells displays either a number, indicating the quantity of mines diagonally and/or adjacent to it, or a blank tile (or "0"), and all adjacent non-mined cells will automatically be uncovered. Right-clicking on a cell will flag it, causing a flag to appear on it. Flagged cells are still considered covered, and a player can click on them to uncover them, although typically they must first be unflagged with an additional right-click.



