package minesweeper;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Grid extends Pane {

    protected static int rows;
    protected static int cols;
    protected static int mines;

    protected static Tile[][] grid;

    public Grid(int rows, int cols, int mines) {
        Grid.rows = rows;
        Grid.cols = cols;
        Grid.mines = mines;

        grid = new Tile[rows][cols];
        setTiles(rows, cols, mines);
        setTranslateX(30);
        setTranslateY(55);
    }

    protected static List<Tile> getNeighbors(Tile tile) {
        List<Tile> neighbors = new ArrayList<>();

        int[] adjacentCoords = new int[]{-1, -1, -1, 0, -1, 1, 0, -1, 0, 1, 1, -1, 1, 0, 1, 1};

        for (int i = 0; i < adjacentCoords.length; i++) {
            int dx = adjacentCoords[i];
            int dy = adjacentCoords[++i];

            int newX = tile.getX() + dx;
            int newY = tile.getY() + dy;

            if (newX >= 0 && newX < rows
                    && newY >= 0 && newY < cols) {
                neighbors.add(grid[newX][newY]);
            }
        }
        return neighbors;
    }

    private void setTiles(int rows, int cols, int mines) {
        Random random = new Random();
        int countBombs = 0;

        for (int y = 0; y < cols; y++) {
            for (int x = 0; x < rows; x++) {
                Tile tile;
                int value = random.nextInt(6);

                if (value >= 5 && countBombs != mines) {
                    //bomb
                    tile = new Tile(x, y, true);
                    countBombs++;
                } else {
                    //safe tile
                    tile = new Tile(x, y, false);
                }
                grid[x][y] = tile;
                getChildren().add(tile);
            }
        }

        for (int y = 0; y < cols; y++) {
            for (int x = 0; x < rows; x++) {
                Tile tile = grid[x][y];

                if (tile.isBomb())
                    continue;
                long bombs = getNeighbors(tile).stream().filter(Tile::isBomb).count();

                //Add values of the tiles
                if (bombs > 0) {
                    tile.getText().setText(String.valueOf(bombs));
                    makeColorValues(tile, bombs);
                }
            }
        }
    }

    private void makeColorValues(Tile tile, long bombs) {
        if(bombs == 1) {
            tile.getText().setFill(Color.BLUE);
        }else if (bombs == 2) {
            tile.getText().setFill(Color.GREEN);
        } else {
            tile.getText().setFill(Color.RED);
        }
    }
}
