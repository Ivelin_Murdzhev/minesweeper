package minesweeper;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;

import java.net.URL;
import java.util.ResourceBundle;

import static minesweeper.Tile.getFlags;

public class Controller implements Initializable {

    protected static Scene scene;
    private static int chosenMines;

    @FXML
    private Pane root;

    @FXML
    private Pane gameBoard;

    @FXML
    private Label minesCounter;

    @FXML
    private ChoiceBox<String> difficulty;

    private final String[] difficulties = {"Beginner", "Intermediate", "Expert"};

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        root.setStyle(("-fx-background-color: #" + "dee0e0"));
        difficulty.getItems().addAll(difficulties);
        difficulty.setOnAction(this::getLevel);
        scene = new Scene(root);
    }

    @FXML
    private void restartGame() {
        gameBoard.getChildren().clear();
        gameBoard = new Grid(Grid.rows, Grid.cols, Grid.mines);
        root.getChildren().add(gameBoard);
    }

    private void getLevel(ActionEvent event) {
        String dif = difficulty.getValue();
        root.getChildren().add(loadGame(dif));
    }

    private Pane loadGame(String difficulty) {
        int x, y, mines;
        switch (difficulty) {
            case "Beginner":
                x = y = 9;
                mines = 10;
                break;
            case "Intermediate":
                x = y = 16;
                mines = 40;
                break;
            case "Expert":
                x = 30;
                y = 16;
                mines = 99;
                break;
            default:
                throw new IllegalArgumentException("Invalid difficulty selected");
        }
        minesCounter.setText(displayMines(mines));
        chosenMines = mines;
        gameBoard.getChildren().clear();
        return gameBoard = new Grid(x, y, mines);
    }

    private String displayMines(int mines) {

        return String.format("Mines: %d", mines - getFlags());
    }
}
