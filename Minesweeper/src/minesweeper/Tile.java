package minesweeper;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.util.Arrays;
import java.util.Optional;

import static minesweeper.Controller.scene;
import static minesweeper.Grid.getNeighbors;

public class Tile extends StackPane {

    private final int TILE_SIZE = 25;
    private final String LOST_GAME = "GAME OVER!";
    private final String WIN_GAME = "YOU SOLVE THE GAME!";

    private final int x;
    private final int y;

    private static int flagCounter = 0;

    private final boolean isBomb;
    private boolean isOpen = false;
    private boolean isFlagged;

    private final Rectangle board = new Rectangle(TILE_SIZE - 3, TILE_SIZE - 3);
    private final Text text = new Text();
    private final Alert alert = new Alert(Alert.AlertType.INFORMATION);

    public Tile(int x, int y, boolean isBomb) {
        this.x = x;
        this.y = y;
        this.isBomb = isBomb;

        this.board.setStroke(Color.DARKBLUE);
        this.text.setFont(Font.font(20));
        this.text.setText(isBomb ? "*" : "");
        this.text.setVisible(false);

        getChildren().addAll(board, text);

        setTranslateX(x * TILE_SIZE);
        setTranslateY(y * TILE_SIZE);

        setOnMouseClicked(event -> {
            if (event.getButton() == MouseButton.PRIMARY) {
                open();
            } else if (event.getButton() == MouseButton.SECONDARY) {
                flagged();
            }
        });
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean isBomb() {
        return isBomb;
    }

    public static int getFlags() {
        return flagCounter;
    }

    protected Text getText() {
        return text;
    }

    protected void open() {
        if (isOpen) {
            return;
        }

        if (isBomb) {
            releaseBombs();
        }

        isOpen = true;
        text.setVisible(true);
        board.setFill(null);
        checkForWin();

        if (text.getText().isEmpty()) {
            getNeighbors(this).forEach(Tile::open);
        }
    }

    private void checkForWin() {
        boolean result = Arrays.stream(Grid.grid)
                .allMatch(tiles -> Arrays.stream(tiles)
                        .allMatch(tile -> tile.isOpen && !tile.isBomb));
        if (result) {
            showAlertBox(WIN_GAME);
        }
    }

    //release the bombs for end game
    private void releaseBombs() {
        for (Tile[] tiles : Grid.grid) {
            for (Tile tile : tiles) {
                if (tile.isBomb && !tile.isOpen) {
                    tile.text.setVisible(true);
                    tile.text.setStroke(Color.RED);
                }
            }
        }
        showAlertBox(LOST_GAME);
    }

    private void flagged() {

        if (isFlagged && !isOpen) {
            board.setStroke(Color.DARKGRAY);
            isFlagged = false;
        }

        if (!isOpen) {
            board.setFill(Color.RED);
            isFlagged = true;
            flagCounter++;
        }
    }

    private void showAlertBox(String score) {
        ButtonType buttonTypeOk = new ButtonType("Play");
        ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);

        if (score.equals(LOST_GAME)) {
            alert.setTitle(LOST_GAME);
            alert.setHeaderText("You lost the game...");

        } else {
            alert.setTitle(WIN_GAME);
            alert.setHeaderText("You win the game * * * ");
        }
        alert.setContentText("Would you play again?");
        alert.getButtonTypes().setAll(buttonTypeOk, buttonTypeCancel);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonTypeOk) {
            //Play again
            scene.setRoot(scene.getRoot());
        } else {
            Platform.exit();
        }
    }

}
